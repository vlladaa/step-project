//our service
const btnsOurService = document.querySelector('.our-service-tabs');
const contentOurService = document.querySelectorAll('.our-service-tabs-content li')

const firstButton = btnsOurService.querySelector('.our-service-tabs-title');
firstButton.classList.add('active');

const firstContent = contentOurService[0];
firstContent.classList.add('active');

btnsOurService.addEventListener('click', (event) => {
    const clickedButton = event.target;
    const id = clickedButton.dataset.text
    const idActive = document.querySelector(id)
    const btnsActive = document.querySelectorAll('.our-service-tabs-title')
    
        if (clickedButton.classList.contains('active')) {
            clickedButton.classList.remove('active');
            idActive.classList.remove('active');
        } else {
            document.querySelectorAll('.our-service-tabs-content li.active').forEach(el => {
                el.classList.remove('active')
            });

            btnsActive.forEach(el => {
                el.classList.remove('active')
            });

            clickedButton.classList.add('active');
            idActive.classList.add('active');
        }
    })

// ---------- Our Amazing Work
const list = document.querySelector('.list')
const blockItems = document.querySelectorAll('.blocks__item')
const listItems = document.querySelectorAll('.list__item')

function filter() {
list.addEventListener('click', event =>{
  const targetId = event.target.dataset.id;
  const target = event.target

  if(target.classList.contains('list__item')){
  listItems.forEach(item => item.classList.remove('active-tab'))
  target.classList.toggle('active-tab')
}

  switch(targetId){
    case 'all': 
  getItems('blocks__item'); 
  break;

      case 'graphic-design':
        getItems(targetId)
      break

      case 'web-design':
        getItems(targetId)
      break

      case 'landeng-pages':
        getItems(targetId)
      break

      case 'wordpress':
        getItems(targetId)
      break
  }
})
}

function getItems(className){
  blockItems.forEach(item => {
    if(item.classList.contains(className)){
      item.style.display = 'block';
    } else {
      item.style.display = 'none'
    }
  })
}

filter()

//------ load more
const loadMoreButton = document.querySelector('.load-more');
loadMoreButton.addEventListener('click', () => {
  const otherDivs = document.querySelectorAll('.other-card')
  otherDivs.forEach(el => {
    el.style.display = 'block'
  })

  loadMoreButton.style.display = 'none'
})

//------- buttons 
document.querySelector('.back-img-wrap').addEventListener('mousedown', (event) =>{
  const activeBtn = event.target;
  if(activeBtn.classList.contains('toggle-img')){
  activeBtn.src = '/img/icon/2.png'
  } else if(activeBtn.classList.contains('toggle-img-second')){{
    activeBtn.src = '/img/icon/4.png'
    activeBtn.style.opacity = '1'
  }}
})

document.querySelector('.back-img-wrap').addEventListener('mouseup', (event) =>{
  const activeBtn = event.target;
  if(activeBtn.classList.contains('toggle-img')){
  activeBtn.src = '/img/icon/Ellipse1.png'
  } else if(activeBtn.classList.contains('toggle-img-second')){{
    activeBtn.src = '/img/icon/Ellipse2.png'
    activeBtn.style.opacity = '1'
  }}
})

//----------- section carousel
$(document).ready(function () {
  $('.owl-carousel').owlCarousel({
      loop:true,
      margin:25,
      nav:true,
      dots:false,
      navText:[
      '<button class="carusel-button prev">&#10095;</button>',
  '<button class="carusel-button next">&#10094;</button>'],
      autoplay: true,
      autoplaySpeed:1500,
      responsive:{
          0:{
              items:4
          },
          
      }
  })
})

const carousel = document.querySelector('.owl-carousel');
let prevActiveSlide = null;

let stopAutopay = () => {
  $(carousel).trigger('stop.owl.autoplay');
}

carousel.addEventListener('mouseenter', stopAutopay, (event) => {
  if(!true){
      event.target.style.transform = 'translateY(0px)';
  } else if(true) {
      event.target.style.transform = 'translateY(10px)';
  }
})

carousel.addEventListener('mouseleave', () => {
  $(carousel).trigger('play.owl.autoplay')
})

carousel.addEventListener('click', (event) => {
const activeSlide = event.target;
if(activeSlide.classList.contains('slide-img')){
if (prevActiveSlide && prevActiveSlide !== activeSlide) {
  $(prevActiveSlide).removeClass('hover-item hover');
  prevActiveSlide.style.transform = 'translateY(0)';
}
$(activeSlide).toggleClass('hover-item').toggleClass('hover');
prevActiveSlide = activeSlide;
}
}, stopAutopay);

const clientText = document.querySelector('.text-wrap');
const clientName = document.querySelector('.profession-text');
const clientProfession = document.querySelector('.profession-text-second')
const clientPhoto = document.querySelector('.people');
const slideTexsts = [
  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magni odio modi perspiciatis ab unde dignissimos veritatis? Assumenda libero saepe sit maxime, et beatae illum labore eaque aspernatur veritatis quam. Debitis sequi odio minus explicabo voluptates voluptate temporibus harum? Dignissimos incidunt, ullam molestiae accusantium autem sapiente dolore temporibus tempora asperiores minus odit aperiam. Perspiciatis, soluta? Neque dolorum rerum recusandae corrupti. Vitae eos laudantium consectetur tempore alias accusamus officiis odit nemo vero. Aut, facilis eum ut quidem doloremque molestias delectus cum ullam quisquam eveniet autem quasi nisi. Nihil ipsum expedita aut!',
  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magni odio modi perspiciatis ab unde dignissimos veritatis? Assumenda libero saepe sit maxime, et beatae illum labore eaque aspernatur veritatis quam. Debitis sequi odio minus explicabo voluptates voluptate temporibus harum? Dignissimos incidunt, ullam molestiae accusantium autem sapiente dolore temporibus tempora asperiores minus odit aperiam. Perspiciatis, soluta? Neque dolorum rerum recusandae corrupti. Vitae eos laudantium consectetur tempore alias accusamus officiis odit nemo vero.',
  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magni odio modi perspiciatis ab unde dignissimos veritatis? Assumenda libero saepe sit maxime, et beatae illum labore eaque aspernatur veritatis quam',
  'Dignissimos incidunt, ullam molestiae accusantium autem sapiente dolore temporibus tempora asperiores minus odit aperiam. Perspiciatis, soluta? Neque dolorum rerum recusandae corrupti. Vitae eos laudantium consectetur tempore alias accusamus officiis odit nemo vero. Aut, facilis eum ut quidem doloremque molestias delectus cum ullam quisquam eveniet autem quasi nisi. Nihil ipsum expedita aut!',
  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magni odio modi perspiciatis ab unde dignissimos veritatis? Assumenda libero saepe sit maxime, et beatae illum labore eaque aspernatur veritatis quam. Debitis sequi odio minus explicabo voluptates voluptate temporibus harum? Dignissimos incidunt, ullam molestiae accusantium autem sapiente dolore temporibus tempora asperiores minus odit aperiam. Perspiciatis, soluta? Neque dolorum rerum recusandae corrupti. Aut, facilis eum ut quidem doloremque molestias delectus cum ullam quisquam eveniet autem quasi nisi. Nihil ipsum expedita aut!',
  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magni odio modi perspiciatis ab unde dignissimos veritatis? Assumenda libero saepe sit maxime, et beatae illum labore eaque aspernatur veritatis quam. Debitis sequi odio minus explicabo voluptates voluptate temporibus harum? Dignissimos incidunt, ullam molestiae accusantium autem sapiente dolore temporibus tempora asperiores minus odit aperiam. Perspiciatis, soluta? Neque dolorum rerum recusandae corrupti. Vitae eos laudantium consectetur tempore alias accusamus officiis odit nemo vero. Aut, facilis eum ut quidem doloremque molestias delectus cum ullam quisquam eveniet autem quasi nisi. Nihil ipsum expedita aut!'
]
const slideProfessions = [
  'Frondend Developer',
  'UX Designer',
  'Software Engineer',
  'Software Engineer',
  'Frondend Developer',
  'UX Designer',
]

const slider = document.querySelector('.owl-carousel');

slider.addEventListener('click', function(event) {
const activeSlide = event.target.closest('.item');
const activeTextIndex = activeSlide.dataset.text;

const slideData = {
  text: slideTexsts[activeTextIndex],
  name: activeSlide.dataset.name,
  image: activeSlide.querySelector('.slide-img').getAttribute('src'),
  profession: slideProfessions[activeTextIndex],
};

clientText.textContent = slideData.text;
clientName.textContent = slideData.name;
clientPhoto.setAttribute('src', slideData.image);
clientProfession.textContent = slideData.profession})
